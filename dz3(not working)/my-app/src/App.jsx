import "./App.css";
import React from "react";
import Navigation from "./components/Navigation/Navigation";
import Home from "./pages/Home";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";

class App extends React.Component {
  state = {
    itemList: [],
    cartItemList: [],
    isBuyModalOpen: false,
    isDeleteModalOpen: false,
    favoriteItems: [],
    currentItemToBuy: "",
  };
  componentDidMount() {
    fetch("./itemsArr.json")
      .then((data) => data.json())
      .then((arr) => {
        this.setState({
          itemList: arr,
        });
      });
    if (localStorage.getItem("cart")) {
      this.setState({ cartItemList: JSON.parse(localStorage.getItem("cart")) });
    }
    if (localStorage.getItem("favoriteItems")) {
      this.setState(() => {
        return {
          favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")),
        };
      });
    }
  }

  handleItemAdd = () => {
    this.setState((prevState) => {
      return { isBuyModalOpen: !prevState.isBuyModalOpen };
    });
    Promise.resolve(
      this.setState((prevState) => {
        if (
          !(
            prevState.cartItemList.indexOf(this.state.currentItemToBuy.name) >
            -1
          )
        ) {
          return {
            cartItemList: [
              ...prevState.cartItemList,
              this.state.currentItemToBuy,
            ],
          };
        }
      })
    ).then(() => {
      localStorage.setItem("cart", JSON.stringify(this.state.cartItemList));
      this.setState({ currentItemToBuy: "" });
    });
  };

  handleFavoriteButtonClick = (item) => {
    Promise.resolve(
      this.setState((prevState) => {
        let favoriteItemsNames = prevState.favoriteItems.map(
          (item) => item.name
        );
        if (!(favoriteItemsNames.indexOf(item.name) > -1)) {
          return { favoriteItems: [...prevState.favoriteItems, item] };
        } else {
          prevState.favoriteItems.splice(
            prevState.favoriteItems.indexOf(item),
            1
          );
          return {
            favoriteItems: prevState.favoriteItems,
          };
        }
      })
    ).then(() => {
      localStorage.setItem(
        "favoriteItems",
        JSON.stringify(this.state.favoriteItems)
      );
    });
  };
  handleModalButtonClick = () => {
    this.setState((prevState) => {
      return { isBuyModalOpen: !prevState.isBuyModalOpen };
    });
  };
  handleItemBuyButtonClick = (item) => {
    this.setState((prevState) => {
      return {
        isBuyModalOpen: !prevState.isBuyModalOpen,
        currentItemToBuy: item,
      };
    });
  };
  handleRemoveModalShow = () => {
    this.setState((prevState) => {
      return { isDeleteModalOpen: !prevState.isDeleteModalOpen };
    });
  };
  handleItemRemove = () => {
    Promise.resolve(
      this.setState((prevState) => {
        return {
          cartItemList: prevState.cartItemList.filter((elem) => {
            console.log(elem.name, this.state.currentItemToBuy.name);

            return elem.name != this.state.currentItemToBuy;
          }),
        };
      })
    ).then(() => {
      this.handleRemoveModalShow();
    });
  };

  render() {
    return (
      <Router>
        <Navigation />
        <Switch>
          {/* <Redirect from="/" to="/home" /> */}
          <Route exact path="/home">
            <Home
              onHandleItemAdd={this.handleItemAdd}
              itemList={this.state.itemList}
              onHandleModalButtonClick={this.handleModalButtonClick}
              onHandleItemBuyButtonClick={this.handleItemBuyButtonClick}
              onHandleFavoriteButtonClick={this.handleFavoriteButtonClick}
              favoriteItems={this.state.favoriteItems}
              isBuyModalOpen={this.state.isBuyModalOpen}
              itemBtnText="Add to Cart"
            />
          </Route>
          <Route path="/favorite">
            <Home
              onHandleItemAdd={this.handleItemAdd}
              itemList={this.state.favoriteItems}
              onHandleModalButtonClick={this.handleModalButtonClick}
              onHandleItemBuyButtonClick={this.handleItemBuyButtonClick}
              onHandleFavoriteButtonClick={this.handleFavoriteButtonClick}
              favoriteItems={this.state.favoriteItems}
              isBuyModalOpen={this.state.isBuyModalOpen}
              itemBtnText="Add to Cart"
            />
          </Route>
          <Route path="/cart">
            <Home
              onHandleItemAdd={this.handleItemRemove}
              itemList={this.state.cartItemList}
              onHandleModalButtonClick={this.handleRemoveModalShow}
              onHandleItemBuyButtonClick={this.handleItemBuyButtonClick}
              onHandleFavoriteButtonClick={this.handleFavoriteButtonClick}
              favoriteItems={this.state.favoriteItems}
              isDeleteModalOpen={this.state.isDeleteModalOpen}
              itemBtnText="Remove from Cart"
            />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
