import React from "react";
import Modal from "../components/Modal/Modal";
import ItemList from "../components/itemList/ItemList";
import Button from "../components/button/Button";

class Home extends React.Component {
  state = {
    currentItemToBuy: "",
  };
  render() {
    return (
      <>
        {this.props.isBuyModalOpen && (
          <Modal
            header="Do you want to add this book to cart?"
            closeButton={true}
            actions={
              <>
                <Button
                  text="add"
                  className="modal__action-button"
                  onClick={() => {
                    this.props.onHandleItemAdd(this.state.currentItemToBuy);
                  }}
                />
                <Button
                  text="close"
                  className="modal__action-button"
                  onClick={() => {
                    this.props.onHandleModalButtonClick();
                  }}
                />
              </>
            }
            onCloseModal={this.props.onHandleModalButtonClick}
          />
        )}
        {this.props.isDeleteModalOpen && (
          <Modal
            header="Do you want to remove this book from cart?"
            closeButton={false}
            actions={
              <>
                <Button
                  text="remove"
                  className="modal__action-button"
                  onClick={() => {
                    this.props.onHandleItemAdd(this.state.currentItemToBuy);
                  }}
                />
                <Button
                  text="close"
                  className="modal__action-button"
                  onClick={() => {
                    this.props.onHandleModalButtonClick();
                  }}
                />
              </>
            }
            onCloseModal={this.props.onHandleModalButtonClick}
          />
        )}
        <ItemList
          itemsArray={this.props.itemList}
          onBuyButtonClick={this.props.onHandleModalButtonClick}
          onItemAdd={this.props.handleItemAdd}
          onCurrentItemChange={this.props.onHandleItemBuyButtonClick}
          onItemBuyButtonClick={this.props.onHandleItemBuyButtonClick}
          onFavoriteButtonClick={this.props.onHandleFavoriteButtonClick}
          favoriteItems={this.props.favoriteItems}
          itemBtnText={this.props.itemBtnText}
        />
      </>
    );
  }
}

export default Home;
