import React from "react";

class Modal extends React.Component {
  HandleModalBoxClick(target) {
    if (target.className === "modal-box") {
      this.props.onCloseModal();
    }
  }
  render() {
    return (
      <div
        onClick={(e) => {
          this.HandleModalBoxClick(e.target);
        }}
        className="modal-box"
      >
        <div className="modal">
          <h2 className="modal__heading">{this.props.header}</h2>
          <p className="modal__description">{this.props.text}</p>
          {this.props.closeButton ? (
            <div
              onClick={this.props.onCloseModal}
              className="modal-closeButton"
            >
              <div className="closeButton-line"></div>
              <div className="closeButton-line"></div>
            </div>
          ) : null}

          <div className="modal__action-btn_container">
            {this.props.actions && this.props.actions}
          </div>
        </div>
      </div>
    );
  }
}
export default Modal;
