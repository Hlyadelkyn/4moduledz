import React from "react";
import PropTypes from "prop-types";

class Button extends React.Component {
  render() {
    return (
      <>
        <button
          className={this.props.className}
          style={{
            background: this.props.backgroundColor,
          }}
          onClick={this.props.onClick}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.any,
  className: PropTypes.string,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
};

export default Button;
