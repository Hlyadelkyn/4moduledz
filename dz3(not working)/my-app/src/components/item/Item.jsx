import React from "react";
import PropTypes from "prop-types";

class Item extends React.Component {
  render() {
    return (
      <div className="item">
        <img src={this.props.imgSrc} alt="item photo" className="item__photo" />
        <h3 className="item__name">{this.props.name}</h3>
        <p className="item__color">colors: {this.props.color}</p>
        <p className="item__article">
          article:{" "}
          <span className="item__article-numbers">{this.props.article}</span>{" "}
        </p>
        <div className="item__price-container">
          <p className="item__price">{this.props.price}</p>
          {this.props.actionBtn}
        </div>
        {this.props.favoriteBtn}
      </div>
    );
  }
}

Item.propTypes = {
  article: PropTypes.number,
  name: PropTypes.string,
  color: PropTypes.string,
  imgSrc: PropTypes.string,
  actionBtn: PropTypes.element,
  favoriteBtn: PropTypes.element,
  price: PropTypes.number,
};

export default Item;
