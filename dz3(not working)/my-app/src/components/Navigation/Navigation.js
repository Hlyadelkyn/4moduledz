import React from "react";
import { Link } from "react-router-dom";

function Navigation() {
  return (
    <>
      <ul className="global-navigation">
        <li className="global-navigation__link">
          <Link to="/home">Home</Link>
        </li>
        <li>
          <Link to="/cart">Cart</Link>
        </li>
        <li>
          <Link to="/favorite">Favorite Books</Link>
        </li>
      </ul>
    </>
  );
}
export default Navigation;
