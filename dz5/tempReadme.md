const [itemList, setItemList] = useState([]);
const [cartItemList, setCartItemList] = useState([]);
const [isBuyModalOpen, setIsBuyModalOpen] = useState(false);
const [isRemoveModalOpen, setIsRemoveModalOpen] = useState(false);
const [favoriteItems, setFavoriteItems] = useState([]);
const [currentItem, setCurrentItem] = useState("");

useEffect(() => {
fetch("./itemsArr.json")
.then((data) => data.json())
.then((arr) => {
setItemList(arr);
});
if (
localStorage.getItem("cart") &&
localStorage.getItem("cart") !== undefined
) {
setCartItemList(JSON.parse(localStorage.getItem("cart")));
}
if (localStorage.getItem("favoriteItems")) {
setFavoriteItems(JSON.parse(localStorage.getItem("favoriteItems")));
}
}, []);

let setLCJSON = (propName, value) => {
localStorage.setItem(propName, JSON.stringify(value));
// console.log(value);
};

let handleItemAdd = () => {
setCartItemList((prevState) => {
if (
!(
cartItemList
.map((item) => item.article)
.indexOf(currentItem.article) > -1
)
) {
return [...prevState, currentItem];
} else {
return prevState;
}
});

    handleBuyModalButtonClick();

};

useEffect(() => {
console.log(cartItemList);
if (cartItemList.length > 0) {
setLCJSON("cart", cartItemList);
}
}, [cartItemList]);

let handleItemRemove = () => {
setCartItemList((prevState) => {
if (prevState)
if (
prevState.map((item) => item.article).indexOf(currentItem.article) >
-1
) {
return prevState.filter((el) => el.article !== currentItem.article);
} else {
return prevState;
}
});
setIsRemoveModalOpen(false);
};

let handleItemBuyButtonClick = (item) => {
setIsBuyModalOpen(true);
setCurrentItem(item);
};
let handleItemRemoveButtonClick = (item) => {
setIsRemoveModalOpen(true);
setCurrentItem(item);
};

let handleBuyModalButtonClick = () => {
setIsBuyModalOpen(false);
};
let handleRemoveModalButtonClick = () => {
setIsRemoveModalOpen(false);
};
let handleFavoriteButtonClick = (item) => {
setFavoriteItems((prevState) => {
if (prevState.map((el) => el.article).indexOf(item.article) < 0) {
return [...prevState, item];
} else {
return prevState.filter((el) => el.article !== item.article);
}
});
};

useEffect(() => {
if (favoriteItems.length > 0) {
setLCJSON("favoriteItems", favoriteItems);
}
}, [favoriteItems]);
