import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

function Navigation() {
  return (
    <ul className="global-nav">
      <li className="global-nav__li">
        <NavLink to="/home">Home</NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/cart">
          cart{" "}
          <span className="itemsCount">
            {" "}
            {useSelector((store) => store.items.cart).length}
          </span>
        </NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/favotite">
          favorite{" "}
          <span className="itemsCount">
            {" "}
            {useSelector((store) => store.items.favorites).length}
          </span>
        </NavLink>
      </li>
    </ul>
  );
}

export default Navigation;
