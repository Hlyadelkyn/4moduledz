import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleRemoveModalState } from "../../state/actions/modals";
import { cartRemoveItemId } from "../../state/actions/items";
import Modal from "../Modal/Modal";
import Button from "../button/Button";

function RemoveModal() {
  let dispatch = useDispatch();
  let isRemoveModalOpen = useSelector(
    (store) => store.modals.isRemoveModalOpen
  );
  return (
    <>
      {isRemoveModalOpen && (
        <Modal
          header="Do you want to remove this book from cart?"
          closeButton={false}
          actions={
            <>
              <Button
                text="remove"
                className="modal__action-button"
                onClick={() => {
                  dispatch(toggleRemoveModalState());
                  dispatch(cartRemoveItemId());
                }}
              />
              <Button
                text="cancel"
                className="modal__action-button"
                onClick={() => {
                  dispatch(toggleRemoveModalState());
                }}
              />
            </>
          }
          onCloseModal={() => {
            dispatch(toggleRemoveModalState());
          }}
        />
      )}
    </>
  );
}
export default RemoveModal;
