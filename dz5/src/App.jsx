import "./App.css";
import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import { useDispatch } from "react-redux";
import Navigation from "./components/navigation/Navigation";

import BuyModal from "./components/Modals/BuyModal";
import RemoveModal from "./components/Modals/RemoveModal";

import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";
import NoPage from "./pages/NoPage";

import { fetchItems } from "./state/actions/items";

const App = () => {
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchItems());
  }, []);
  return (
    <Router>
      <div className="App">
        <BuyModal />
        <RemoveModal />

        <Navigation />
        <Switch>
          <Redirect exact from="/" to="/home" />

          <Route path="/home">
            <Home />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/favotite">
            <Favorites />
          </Route>
          <Route path="/*">
            <NoPage />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};
export default App;
