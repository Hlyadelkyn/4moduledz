import { reducer as itemsReducer } from "./items";
import { reducer as modalsReducer } from "./modals";
import { combineReducers } from "redux";

export const reducer = combineReducers({
  items: itemsReducer,
  modals: modalsReducer,
});
