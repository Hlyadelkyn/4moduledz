import {
  FETCH_ITEMS_BEGIN,
  FETCH_ITEMS_FAILURE,
  FETCH_ITEMS_SUCCESS,
  CURRENT_ITEMid_SET,
  CART_ADD_ITEMid,
  CART_REMOVE_ITEMid,
  FAVORITES_TOGGLEid,
  CART_SET_IDLIST,
} from "../actions/items";

const initialState = {
  items: [],
  isLoading: false,
  isError: false,
  error: null,
  currentItemId: null,
  cart: localStorage.getItem("cart")
    ? JSON.parse(localStorage.getItem("cart"))
    : [],
  favorites: localStorage.getItem("favorites")
    ? JSON.parse(localStorage.getItem("favorites"))
    : [],
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEMS_BEGIN:
      return { ...state, isLoading: true };
    case FETCH_ITEMS_FAILURE:
      return { ...state, isLoading: false, items: action.payload };
    case FETCH_ITEMS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        items: action.payload,
      };
    case CURRENT_ITEMid_SET:
      console.log(action.payload);
      return {
        ...state,
        currentItemId: action.payload,
      };

    case CART_ADD_ITEMid:
      if (!state.cart.includes(action.payload)) {
        localStorage.setItem(
          "cart",
          JSON.stringify([...state.cart, action.payload])
        );
        return {
          ...state,
          cart: [...state.cart, action.payload],
        };
      } else {
        return state;
      }

    case CART_REMOVE_ITEMid:
      localStorage.setItem(
        "cart",
        JSON.stringify(state.cart.filter((el) => el !== action.payload))
      );
      return {
        ...state,
        cart: state.cart.filter((el) => el !== action.payload),
      };
    case CART_SET_IDLIST:
      localStorage.setItem("cart", action.payload);
      return {
        ...state,
        cart: action.payload,
      };
    case FAVORITES_TOGGLEid:
      if (!state.favorites.includes(action.payload)) {
        localStorage.setItem(
          "favorites",
          JSON.stringify([...state.favorites, action.payload])
        );
        return {
          ...state,
          favorites: [...state.favorites, action.payload],
        };
      } else {
        localStorage.setItem(
          "favorites",
          JSON.stringify(state.favorites.filter((id) => id !== action.payload))
        );
        return {
          ...state,
          favorites: state.favorites.filter((id) => id !== action.payload),
        };
      }
    default:
      return state;
  }
};
