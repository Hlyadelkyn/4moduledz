import PropTypes from "prop-types";

function Button(props) {
  return (
    <>
      <button
        data-testid="button"
        className={props.className}
        style={{
          background: props.backgroundColor,
        }}
        onClick={props.onClick}
      >
        {props.text}
      </button>
    </>
  );
}

Button.propTypes = {
  onClick: PropTypes.any,
  className: PropTypes.string,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
};

export default Button;
