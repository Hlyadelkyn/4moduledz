import PropTypes from "prop-types";

function Item(props) {
  return (
    <div className={props.className}>
      <img src={props.imgSrc} alt="item photo" className="item__photo" />
      <h3 className="item__name">{props.name}</h3>
      <p className="item__color">colors: {props.color}</p>
      <p className="item__article">
        article: <span className="item__article-numbers">{props.article}</span>{" "}
      </p>
      <div className="item__price-container">
        <p className="item__price">{props.price}</p>
        {props.actionBtn}
      </div>
      {props.favoriteBtn}
    </div>
  );
}

Item.propTypes = {
  article: PropTypes.number,
  name: PropTypes.string,
  color: PropTypes.string,
  imgSrc: PropTypes.string,
  actionBtn: PropTypes.element,
  favoriteBtn: PropTypes.element,
  price: PropTypes.number,
};

export default Item;
