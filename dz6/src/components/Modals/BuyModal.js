import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleBuyModalState } from "../../state/actions/modals";
import { cartAddItemId } from "../../state/actions/items";
import Modal from "../Modal/Modal";
import Button from "../button/Button";

function BuyModal() {
  let dispatch = useDispatch();
  let isBuyModalOpen = useSelector((store) => store.modals.isBuyModalOpen);
  return (
    <>
      {isBuyModalOpen && (
        <Modal
          header="Do you want to add this book to cart?"
          closeButton={true}
          actions={
            <>
              <Button
                text="add"
                className="modal__action-button"
                onClick={() => {
                  dispatch(toggleBuyModalState());
                  dispatch(cartAddItemId());
                }}
              />
              <Button
                text="cancel"
                className="modal__action-button"
                onClick={() => {
                  dispatch(toggleBuyModalState());
                }}
              />
            </>
          }
          onCloseModal={() => {
            dispatch(toggleBuyModalState());
          }}
        />
      )}
    </>
  );
}
export default BuyModal;
