import React from "react";
import { useDispatch } from "react-redux";
import { cartSetIdList } from "../../state/actions/items";
import { useFormik } from "formik";
import * as Yup from "yup";
import { PatternFormat } from "react-number-format";

function BuyForm({ itemsArray }) {
  const dispatch = useDispatch();

  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  let formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      adress: "",
      phoneNumber: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required"),

      lastName: Yup.string()
        .max(20, "Must be 20 characters or less")
        .required("Required"),

      adress: Yup.string().required("Required"),
      age: Yup.number()
        .max(99, "Must be 99 or less")
        .min(9, "Must be at least 9 ")
        .required("Required"),
      phoneNumber: Yup.string().required("Required"),
    }),
    onSubmit: (values) => {
      console.log(
        "in your cart:",
        ...itemsArray.map((el) => {
          return `${el.name},`;
        }),
        "thanks for your order, contac information: ",
        ...Object.values(values)
      );
      dispatch(cartSetIdList([]));
    },
  });

  return (
    <>
      <div className="buy-form__wrapper">
        <h2>Buy</h2>
        <form onSubmit={formik.handleSubmit} className="buy-form">
          <div className="buy-form__input-container">
            <label htmlFor="firstName">First Name</label>

            <input
              id="firstName"
              name="firstName"
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.firstName}
            />

            {formik.touched.firstName && formik.errors.firstName ? (
              <span className="form__error-text">
                {formik.errors.firstName}
              </span>
            ) : null}
          </div>
          <div className="buy-form__input-container">
            {" "}
            <label htmlFor="lastName">Last Name</label>
            <input
              id="lastName"
              name="lastName"
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.lastName}
            />
            {formik.touched.lastName && formik.errors.lastName ? (
              <span className="form__error-text">{formik.errors.lastName}</span>
            ) : null}
          </div>
          <div className="buy-form__input-container">
            <label htmlFor="age">Age</label>

            <input
              id="age"
              name="age"
              type="number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.age}
            />

            {formik.touched.age && formik.errors.age ? (
              <span className="form__error-text">{formik.errors.age}</span>
            ) : null}
          </div>
          <div className="buy-form__input-container">
            <label htmlFor="adress"> Address</label>

            <input
              id="adress"
              name="adress"
              type="adress"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.adress}
            />

            {formik.touched.adress && formik.errors.adress ? (
              <span className="form__error-text">{formik.errors.adress}</span>
            ) : null}
          </div>
          <div className="buy-form__input-container">
            <label htmlFor="phoneNumber">Phone</label>

            <PatternFormat
              type="tel"
              id="phoneNumber"
              name="phoneNumber"
              onChange={formik.handleChange}
              value={formik.values.phoneNumber}
              format="(###)###-##-##"
              onBlur={formik.handleBlur}
            />

            {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
              <span className="form__error-text">
                {formik.errors.phoneNumber}
              </span>
            ) : null}
          </div>

          <button type="submit">Checkout</button>
        </form>
      </div>
    </>
  );
}

export default BuyForm;
