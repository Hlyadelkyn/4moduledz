import { ItemlistModeContext } from "./ItemlistModeContext";
import Button from "../components/button/Button";

function ItemsTogglerBtn() {
  return (
    <ItemlistModeContext.Consumer>
      {({ mode, toggleMode }) => (
        <Button
          onClick={toggleMode}
          className={`toggler`}
          text={mode == "list" ? "cards" : "list"}
        ></Button>
      )}
    </ItemlistModeContext.Consumer>
  );
}

export default ItemsTogglerBtn;
