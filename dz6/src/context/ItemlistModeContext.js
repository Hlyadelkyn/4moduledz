import React from "react";

export const modeContext = {
  cards: "cards",
  list: "list",
};

export const ItemlistModeContext = React.createContext({
  mode: modeContext.cards,
  toggleMode: () => {},
});
