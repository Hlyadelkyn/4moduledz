import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Button from "../../components/button/Button";

describe("Test Button component ", () => {
  it("should render without props", () => {
    render(<Button />);
  });
  it("should render with incorrect props", () => {
    render(<Button text={true} onClick={undefined} className={5} />);
  });
  it("should render text properly", () => {
    render(<Button text="add" className="modal__action" />);
    const content = screen.getByTestId("button").textContent;
    expect(content).toBe("add");
  });
  it("should run prop func", () => {
    render(<Button onClick={() => {}} />);
    const btn = screen.getByTestId("button");
    userEvent.click(btn);
    //   намагався зімітувати клік користувача через бібліотеку але не знайшов способу як отримати відводь функції якщо вона не змінює компонент візуально
  });
  it("should render svg", () => {
    render(
      <Button
        text={
          <svg
            className="favorite-button_svg "
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
          >
            <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
          </svg>
        }
      />
    );
  });
});
