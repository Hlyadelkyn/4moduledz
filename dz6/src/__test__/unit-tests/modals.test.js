import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../state/store";

import Modal from "../../components/Modal/Modal";
import RemoveModal from "../../components/Modals/RemoveModal";
import BuyModal from "../../components/Modals/BuyModal";

describe("testing modals,", () => {
  describe("Test default Modal", () => {
    it("should render without props", () => {
      render(<Modal />);
    });
    it("should render text properly", () => {
      render(<Modal header="test header" />);
      const content = screen.getByTestId("modal").textContent;
      expect(content).toBe("test header");
    });
    it("should render with incorrect props", () => {
      render(
        <Modal header={3} text={true} actions="actions" onCloseModal="close" />
      );
    });
  });
  describe("Test RemoveModal component", () => {
    it("should render", () => {
      <Provider store={store}>
        render(
        <RemoveModal />
        );
      </Provider>;
    });
  });
  describe("Test BuyModal component", () => {
    it("should render", () => {
      <Provider store={store}>
        render(
        <BuyModal />
        );
      </Provider>;
    });
  });
});
