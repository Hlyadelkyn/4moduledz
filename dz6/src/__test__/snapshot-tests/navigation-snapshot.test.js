import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import store from "../../state/store";

import Navigation from "../../components/navigation/Navigation";

it("Navigation renders correctly", () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <Router>
          <Navigation />
        </Router>
      </Provider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
