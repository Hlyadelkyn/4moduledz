import renderer from "react-test-renderer";

import Item from "../../components/item/Item";

import Button from "../../components/button/Button";

it("Item renders correctly", () => {
  const tree = renderer
    .create(
      <Item
        className={"item-list"}
        key={14}
        name={"1984"}
        price={300}
        imgSrc={"mockSrc"}
        article={10441}
        color={"red"}
        favoriteBtn={
          <Button
            text={
              <svg
                className="favorite-button_svg "
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
              </svg>
            }
            className={"isInFavorites"}
          />
        }
        actionBtn={
          <Button text={"props.itemButtonText"} className="item__buy-btn" />
        }
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
