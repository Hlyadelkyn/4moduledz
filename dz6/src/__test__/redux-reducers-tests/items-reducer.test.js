import { reducer } from "../../state/reducers/items";

import { fetchItems } from "../../state/actions/items";

describe("items reducer", () => {
  it("fetches items", async () => {
    const initialState = {
      items: [],
      isLoading: false,
      isError: false,
      error: null,
    };
    let newState = await reducer(initialState, fetchItems());
    console.log(newState);
  });
  // намагався зробити ред'юсер
});

describe("cart items reducer", () => {
  it("sets the new cart value", () => {
    const initialState = {
      cart: [],
    };
    const action = { type: "CART_SET_IDLIST", payload: [10000, 10001] };

    const newState = reducer(initialState, action);

    expect(newState.cart).toHaveLength(2);
  });
  it("adds item to cart", () => {
    const initialState = {
      currentItemId: null,
      cart: [],
    };
    const action = { type: "CART_ADD_ITEMid", payload: 10000 };

    const newState = reducer(initialState, action);

    expect(newState.cart).toHaveLength(1);
    expect(newState.cart[0]).toEqual(10000);
  });
  it("removes item from cart", () => {
    const initialState = {
      currentItemId: null,
      cart: [10000],
    };
    const action = { type: "CART_REMOVE_ITEMid", payload: 10000 };

    const newState = reducer(initialState, action);

    expect(newState.cart).toHaveLength(0);
  });
});

describe("favorite items reducer", () => {
  it("toggles item in favorites", () => {
    const initialState = {
      favorites: [],
    };
    const action = { type: "FAVORITES_TOGGLEid", payload: 10000 };

    const firstState = reducer(initialState, action);
    const secondState = reducer(firstState, action);

    expect(firstState.favorites).toHaveLength(1);
    expect(secondState.favorites).toHaveLength(0);
  });
});
describe("current item reducer", () => {
  it("sets item to current", () => {
    const initialState = {
      currentItemId: null,
    };
    const action = { type: "CURRENT_ITEMid_SET", payload: 10101 };

    const newState = reducer(initialState, action);

    expect(newState.currentItemId).toBe(10101);
  });
});
