import { reducer } from "../../state/reducers/modals";

describe("modals reducer", () => {
  it("toggles buy modal state", () => {
    let initialState = {
      isBuyModalOpen: false,
    };
    let action = { type: "TOGGLE_BUY_MODAL_STATE" };

    let newState = reducer(initialState, action);
    let secondState = reducer(newState, action);

    console.log(initialState, newState, secondState);

    expect(newState.isBuyModalOpen).toBe(true);
    expect(secondState.isBuyModalOpen).toBe(false);
  });
  it("toggles remove modal state", () => {
    let initialState = {
      isRemoveModalOpen: false,
    };
    let action = { type: "TOGGLE_REMOVE_MODAL_STATE" };

    let newState = reducer(initialState, action);
    let secondState = reducer(newState, action);

    console.log(initialState, newState, secondState);

    expect(newState.isRemoveModalOpen).toBe(true);
    expect(secondState.isRemoveModalOpen).toBe(false);
  });
});
