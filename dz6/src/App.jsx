import "./App.css";
import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import { useDispatch } from "react-redux";
import Navigation from "./components/navigation/Navigation";
import ItemsTogglerBtn from "./context/ItemsTogglerBtn";

import BuyModal from "./components/Modals/BuyModal";
import RemoveModal from "./components/Modals/RemoveModal";

import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";
import NoPage from "./pages/NoPage";

import { fetchItems } from "./state/actions/items";

import { ItemlistModeContext } from "./context/ItemlistModeContext";
import { modeContext } from "./context/ItemlistModeContext";

const App = () => {
  let dispatch = useDispatch();
  const [appModeContext, setAppModeContext] = useState(modeContext.cards);
  let toggleModeContext = () => {
    if (appModeContext == "list") {
      setAppModeContext(modeContext.cards);
    } else {
      setAppModeContext(modeContext.list);
    }
  };
  useEffect(() => {
    dispatch(fetchItems());
  }, []);

  return (
    <ItemlistModeContext.Provider
      value={{ mode: appModeContext, toggleMode: toggleModeContext }}
    >
      <Router>
        <div className="App">
          <BuyModal />
          <RemoveModal />

          <Navigation />
          <ItemsTogglerBtn />

          <Switch>
            <Redirect exact from="/" to="/home" />

            <Route path="/home">
              <Home />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/favotite">
              <Favorites />
            </Route>
            <Route path="/*">
              <NoPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </ItemlistModeContext.Provider>
  );
};
export default App;
