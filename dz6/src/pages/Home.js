import React from "react";
import { useSelector } from "react-redux";

import ItemList from "../components/itemList/ItemList";

import { toggleBuyModalState } from "../state/actions/modals";

function Home() {
  let itemList = useSelector((store) => store.items.items);

  return (
    <>
      <h1 className="page-title">Store</h1>
      <ItemList
        itemsArray={itemList}
        onItemButtonClick={toggleBuyModalState}
        itemButtonText="add to cart"
      />
    </>
  );
}
export default Home;
