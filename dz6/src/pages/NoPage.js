import React from "react";

function NoPage() {
  return (
    <>
      <h4>Page not found</h4>
    </>
  );
}
export default NoPage;
