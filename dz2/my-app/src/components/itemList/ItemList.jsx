import React from "react";
import Item from "../item/Item";
import Button from "../button/Button";
import PropTypes from "prop-types";

class ItemList extends React.Component {
  render() {
    return (
      <>
        <div className="items-container">
          {this.props.itemsArray.map((item, id) => {
            let isInFavorites;
            if (this.props.favoriteItems.indexOf(item.name) !== -1) {
              isInFavorites = "item__favorite-button inFavorites";
            } else {
              isInFavorites = "item__favorite-button";
            }
            return (
              <Item
                key={id}
                name={item.name}
                price={item.price}
                imgSrc={item.img}
                article={item.article}
                color={item.color}
                favoriteBtn={
                  <Button
                    text={
                      <svg
                        className="favorite-button_svg "
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
                      </svg>
                    }
                    className={isInFavorites}
                    onClick={() => {
                      this.props.onFavoriteButtonClick(item.name);
                    }}
                  />
                }
                actionBtn={
                  <Button
                    text="add to cart"
                    className="item__buy-btn"
                    onClick={() => {
                      this.props.onItemBuyButtonClick(item.name);
                    }}
                  />
                }
              />
            );
          })}
        </div>
      </>
    );
  }
}

ItemList.propTypes = {
  favoriteItems: PropTypes.array,
  itemsArray: PropTypes.array,
  onBuyButtonClick: PropTypes.func,
  onCurrentItemChange: PropTypes.func,
  onFavoriteButtonClick: PropTypes.func,
  onItemAdd: PropTypes.func,
  onItemBuyButtonClick: PropTypes.func,
};

export default ItemList;
