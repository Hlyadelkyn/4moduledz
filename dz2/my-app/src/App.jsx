import "./App.css";
import React from "react";
import ItemList from "./components/itemList/ItemList";
import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";

class App extends React.Component {
  state = {
    itemList: [],
    cartItemList: [],
    isBuyModalOpen: false,
    favoriteItems: [],
    currentItemToBuy: "",
  };
  componentDidMount() {
    fetch("./itemsArr.json")
      .then((data) => data.json())
      .then((arr) => {
        this.setState({
          itemList: arr,
        });
      });
    if (localStorage.getItem("cart")) {
      this.setState({ cartItemList: JSON.parse(localStorage.getItem("cart")) });
    }
    if (localStorage.getItem("favoriteItems")) {
      this.setState(() => {
        return {
          favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")),
        };
      });
    }
  }

  handleItemAdd = (item) => {
    this.setState((prevState) => {
      return { isBuyModalOpen: !prevState.isBuyModalOpen };
    });
    Promise.resolve(
      this.setState((prevState) => {
        if (!(prevState.cartItemList.indexOf(item) > -1)) {
          return { cartItemList: [...prevState.cartItemList, item] };
        }
      })
    ).then(() => {
      localStorage.setItem("cart", JSON.stringify(this.state.cartItemList));
      this.setState({ currentItemToBuy: "" });
    });
  };

  handleFavoriteButtonClick = (name) => {
    Promise.resolve(
      this.setState((prevState) => {
        if (!(prevState.favoriteItems.indexOf(name) > -1)) {
          return { favoriteItems: [...prevState.favoriteItems, name] };
        } else {
          prevState.favoriteItems.splice(
            prevState.favoriteItems.indexOf(name),
            1
          );
          return {
            favoriteItems: prevState.favoriteItems,
          };
        }
      })
    ).then(() => {
      localStorage.setItem(
        "favoriteItems",
        JSON.stringify(this.state.favoriteItems)
      );
    });
  };
  handleModalButtonClick = () => {
    this.setState((prevState) => {
      return { isBuyModalOpen: !prevState.isBuyModalOpen };
    });
  };
  handleItemBuyButtonClick = (name) => {
    this.setState((prevState) => {
      return {
        isBuyModalOpen: !prevState.isBuyModalOpen,
        currentItemToBuy: name,
      };
    });
  };

  render() {
    return (
      <div className="App">
        {this.state.isBuyModalOpen && (
          <Modal
            header="Do you want to add this book to cart?"
            closeButton={true}
            actions={
              <>
                <Button
                  text="add"
                  className="modal__action-button"
                  onClick={() => {
                    this.handleItemAdd(this.state.currentItemToBuy);
                  }}
                />
                <Button
                  text="close"
                  className="modal__action-button"
                  onClick={() => {
                    this.handleModalButtonClick();
                  }}
                />
              </>
            }
            onCloseModal={this.handleModalButtonClick}
          />
        )}
        <ItemList
          itemsArray={this.state.itemList}
          onBuyButtonClick={this.handleModalButtonClick}
          onItemAdd={this.handleItemAdd}
          onCurrentItemChange={this.handleItemBuyButtonClick}
          onItemBuyButtonClick={this.handleItemBuyButtonClick}
          onFavoriteButtonClick={this.handleFavoriteButtonClick}
          favoriteItems={this.state.favoriteItems}
        />
      </div>
    );
  }
}

export default App;
