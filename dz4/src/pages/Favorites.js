import React from "react";
import { useSelector } from "react-redux";

import ItemList from "../components/itemList/ItemList";

import { toggleBuyModalState } from "../state/actions/modals";

function Favorites() {
  let itemList = useSelector((store) => store.items.items);
  let favoritesId = useSelector((store) => store.items.favorites);

  return (
    <>
      <h1 className="page-title">Favorites</h1>
      <ItemList
        itemsArray={itemList.filter((el) => {
          return favoritesId.includes(el.article);
        })}
        onItemButtonClick={toggleBuyModalState}
        itemButtonText="add to cart"
      />
    </>
  );
}
export default Favorites;
