import React from "react";
import { useSelector } from "react-redux";

import ItemList from "../components/itemList/ItemList";

import { toggleRemoveModalState } from "../state/actions/modals";

function Cart() {
  let itemList = useSelector((store) => store.items.items);
  let cartItemsId = useSelector((store) => store.items.cart);

  return (
    <>
      <h1 className="page-title">Cart</h1>
      <ItemList
        itemsArray={itemList.filter((el) => {
          return cartItemsId.includes(el.article);
        })}
        onItemButtonClick={toggleRemoveModalState}
        itemButtonText="remove from cart"
      />
    </>
  );
}
export default Cart;
