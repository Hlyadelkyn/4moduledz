export const TOGGLE_BUY_MODAL_STATE = "TOGGLE_BUY_MODAL_STATE";
export const TOGGLE_REMOVE_MODAL_STATE = "TOGGLE_REMOVE_MODAL_STATE";

export const toggleBuyModalState = () => {
  return (dispatch) => {
    dispatch({ type: TOGGLE_BUY_MODAL_STATE });
  };
};

export const toggleRemoveModalState = () => {
  return (dispatch) => dispatch({ type: TOGGLE_REMOVE_MODAL_STATE });
};
