export const FETCH_ITEMS_BEGIN = "FETCH_ITEMS_BEGIN";
export const FETCH_ITEMS_SUCCESS = "FETCH_ITEMS_SUCCESS";
export const FETCH_ITEMS_FAILURE = "FETCH_ITEMS_FAILURE";
export const CURRENT_ITEMid_SET = "CURRENT_ITEMid_SET";
export const CART_ADD_ITEMid = "CART_ADD_ITEMid";
export const CART_REMOVE_ITEMid = "CART_REMOVE_ITEMid";
export const FAVORITES_TOGGLEid = "FAVORITES_TOGGLEid";

export const fetchItemsBegin = () => {
  return (dispatch) => dispatch({ type: FETCH_ITEMS_BEGIN });
};
export const fetchItemsSuccess = (data) => {
  return (dispatch) => dispatch({ type: FETCH_ITEMS_SUCCESS, payload: data });
};
export const fetchItemsFailure = (error) => {
  return (dispatch) => dispatch({ type: FETCH_ITEMS_FAILURE, payload: error });
};

export const fetchItems = () => {
  return (dispatch) => {
    dispatch(fetchItemsBegin());
    fetch("./itemsArr.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("SERVER ERROR");
        } else {
          return response;
        }
      })
      .then((resp) => resp.json())
      .then((data) => dispatch(fetchItemsSuccess(data)))
      .catch((err) => dispatch(fetchItemsFailure(err)));
  };
};
export const setCurrentItemId = (id) => {
  return (dispatch) => dispatch({ type: CURRENT_ITEMid_SET, payload: id });
};
export const cartAddItemId = () => {
  return (dispatch, getState) => {
    let currentItemId = getState().items.currentItemId;
    dispatch({ type: CART_ADD_ITEMid, payload: currentItemId });
  };
};
export const cartRemoveItemId = () => {
  return (dispatch, getState) => {
    let currentItemId = getState().items.currentItemId;
    dispatch({ type: CART_REMOVE_ITEMid, payload: currentItemId });
  };
};
export const favoritesToggleId = (id) => {
  return (dispatch) => {
    dispatch({
      type: FAVORITES_TOGGLEid,
      payload: id,
    });
  };
};
