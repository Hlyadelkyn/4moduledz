import {
  TOGGLE_BUY_MODAL_STATE,
  TOGGLE_REMOVE_MODAL_STATE,
} from "../actions/modals";

const initialState = {
  isBuyModalOpen: false,
  isRemoveModalOpen: false,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_BUY_MODAL_STATE:
      return { ...state, isBuyModalOpen: !state.isBuyModalOpen };
    case TOGGLE_REMOVE_MODAL_STATE:
      return { ...state, isRemoveModalOpen: !state.isRemoveModalOpen };

    default:
      return state;
  }
};
