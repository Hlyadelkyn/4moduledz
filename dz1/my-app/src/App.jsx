import "./App.scss";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
  state = {
    isOpen1: false,
    isOpen2: false,
  };
  // handleButtonClick = (id) => {
  //   this.setState((prevState)=>{
  //     ["isOpen" + id]: !prevState["isOpen"+id],
  //   });
  // };
  handleButtonClick1 = () => {
    this.setState((prevState) => {
      return { isOpen1: !prevState.isOpen1 };
    });
  };
  handleButtonClick2 = () => {
    this.setState((prevState) => {
      return { isOpen2: !prevState.isOpen2 };
    });
  };
  render() {
    return (
      <div className="App">
        <Button
          onClick={() => {
            this.handleButtonClick1();
          }}
          backgroundColor="#6A8D92"
          text="Open first modal"
          className="button"
        />

        <Button
          onClick={() => {
            this.handleButtonClick2();
          }}
          backgroundColor="#A1E887"
          text="Open second modal"
          className="button"
        />

        {this.state.isOpen1 && (
          <Modal
            header="Do you want to delete this files?"
            closeButton={false}
            text="Once you get this file,it wont be possible to send this files. Are you sure you want to get them?"
            actions={
              <>
                <Button
                  text="delete"
                  className="modal__action-button"
                  onClick={() => {
                    this.handleButtonClick1();
                  }}
                />
                <Button
                  text="close"
                  className="modal__action-button"
                  onClick={() => {
                    this.handleButtonClick1();
                  }}
                />
              </>
            }
            onCloseModal={this.handleButtonClick1}
          />
        )}

        {this.state.isOpen2 && (
          <Modal
            header="Do you want to get this files?"
            closeButton={true}
            text="Once you get this file,it wont be possible to send this files. Are you sure you want to get them?"
            actions={
              <>
                <Button
                  text="get"
                  className="modal__action-button"
                  onClick={() => {
                    this.handleButtonClick2();
                  }}
                />
              </>
            }
            onCloseModal={this.handleButtonClick2}
          />
        )}
      </div>
    );
  }
}

export default App;
