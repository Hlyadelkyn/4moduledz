export function setCartItemsIdList() {
  if (
    localStorage.getItem("cart") &&
    localStorage.getItem("cart") !== undefined
  ) {
    return {
      type: "cartItemsIdList/setList",
      payload: JSON.parse(localStorage.getItem("cart")),
    };
  } else {
    return {
      type: "cartItemsIdList/setList",
      payload: [],
    };
  }
}

export function addItemIdtoCart(item) {
  return {
    type: "cartItemsIdList/addId",
    payload: item.article,
  };
}
export function removeItemIdfromCart(item) {
  return {
    type: "cartItemsIdList/removeId",
    payload: item.article,
  };
}
