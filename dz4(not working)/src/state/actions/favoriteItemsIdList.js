export function setFavoriteItemsIdList() {
  if (localStorage.getItem("favoriteItems")) {
    return {
      type: "favoriteItemsIdList/setList",
      payload: JSON.parse(localStorage.getItem("favoriteItems")),
    };
  } else {
    return {
      type: "favoriteItemsIdList/setList",
      payload: [],
    };
  }
}

export function toggleFavoriteItemId(item) {
  return {
    type: "favoriteItemsIdList/toggleId",
    payload: item.article,
  };
}
