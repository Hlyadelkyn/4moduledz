export function setItemsList() {
  return async (dispatch) => {
    let data = await fetch("./itemsArr.json");
    let resp = await data.json();
    dispatch({
      type: "itemsList/setList",
      payload: resp,
    });
  };
}
export function setCurrentItemId(id) {
  return {
    type: "currentItemId/set",
    payload: id,
  };
}
