import "./App.css";
import React, { useEffect } from "react";

import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";
import Navigation from "./components/navigation/Navigation";

import store from "./store";

import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";

import Cart from "./pages/Cart";
import Home from "./pages/Home";
import Favorite from "./pages/Favorite";

import {
  setCartItemsIdList,
  addItemIdtoCart,
  removeItemIdFromCart,
} from "./state/actions/cartItemsIdList";
import { setItemsList } from "./state/actions/itemsList";
import { setFavoriteItemsIdList } from "./state/actions/favoriteItemsIdList";
import {
  buyModalSetState,
  removeModalSetState,
} from "./state/actions/modalsHandlers";

import { useStore, useDispatch } from "react-redux";

const App = () => {
  // let store = useStore();
  let dispatch = useDispatch();

  let isBuyModalOpen = store.getState().isBuyModalOpen;
  let isRemoveModalOpen = store.getState().isRemoveModalOpen;

  useEffect(() => {
    dispatch(setItemsList());
    dispatch(setCartItemsIdList());
    dispatch(setFavoriteItemsIdList());
  }, []);

  return (
    <Router>
      <div className="App">
        <Navigation />
        <Switch>
          <Redirect exact from="/" to="/home" />

          <Route path="/home">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          addItemIdtoCart(store.getState().currentItem);
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          buyModalSetState(false);
                        }}
                      />
                    </>
                  }
                  onCloseModal={buyModalSetState(false)}
                />
              )}
              <h1 className="page-title">Store</h1>

              <Home />
            </>
          </Route>
          <Route path="/cart">
            {isRemoveModalOpen && (
              <Modal
                header="Do you want to remove this book from cart?"
                closeButton={false}
                actions={
                  <>
                    <Button
                      text="remove"
                      className="modal__action-button"
                      onClick={() => {
                        // removeItemIdFromCart(store.getState().currentItem);
                      }}
                    />
                    <Button
                      text="cancel"
                      className="modal__action-button"
                      onClick={() => {
                        removeModalSetState(false);
                      }}
                    />
                  </>
                }
                onCloseModal={() => {
                  removeModalSetState(false);
                }}
              />
            )}
            <h1 className="page-title">Cart</h1>

            <Cart />
          </Route>
          <Route path="/favotite">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          addItemIdtoCart(store.getState().currentItem);
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          buyModalSetState(false);
                        }}
                      />
                    </>
                  }
                  onCloseModal={() => {
                    buyModalSetState(false);
                  }}
                />
              )}
              <h1 className="page-title">Favorite</h1>

              <Favorite />
            </>
          </Route>
          <Route path="/*">
            <>
              <h1 className="not-found">oops, smth went wrong</h1>
            </>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};
export default App;
