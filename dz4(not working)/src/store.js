import { createStore, compose, applyMiddleware } from "redux";

import thunk from "redux-thunk";

const initialState = {
  itemsList: [],
  cartItemsIdList: [],
  isBuyModalOpen: false,
  isRemoveModalOpen: false,
  favoriteItemsIdList: [],
  currentItemId: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "itemsList/setList":
      return {
        ...state,
        itemsList: action.payload,
      };
    case "cartItemsIdList/setList":
      return {
        ...state,
        cartItemsIdList: action.payload,
      };
    case "cartItemsIdList/addId":
      return {
        ...state,
        cartItemsIdList: [...state.cartItemsIdList, action.payload],
      };
    case "cartItemsIdList/removeId":
      return {
        ...state,
        cartItemsIdList: state.cartItemsIdList.filter(
          (item) => item !== action.payload
        ),
      };
    case "favoriteItemsIdList/setList":
      return {
        ...state,
        favoriteItemsIdList: action.payload,
      };
    case "favoriteItemsIdList/toggleId":
      if (
        state.favoriteItemsIdList[
          state.favoriteItemsIdList.indexOf(action.payload)
        ] > -1
      ) {
        return {
          ...state,
          favoriteItemsIdList: [
            ...state.cartItemsIdList.filter((id) => id !== action.payload),
          ],
        };
      } else {
        return {
          ...state,
          favoriteItemsIdList: [...state.favoriteItemsIdList, action.payload],
        };
      }
    case "currentItemId/set":
      return {
        ...state,
        currentItemId: action.payload,
      };

    case "buyModal/setState":
      return {
        ...state,
        isBuyModalOpen: action.payload,
      };
    default:
      return state;
  }
};
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancer(applyMiddleware(thunk)));

export default store;
