import React from "react";
import PropTypes from "prop-types";

function Modal(props) {
  let HandleModalBoxClick = (target) => {
    if (target.className === "modal-box") {
      props.onCloseModal();
    }
  };

  return (
    <div
      onClick={(e) => {
        HandleModalBoxClick(e.target);
      }}
      className="modal-box"
    >
      <div className="modal">
        <h2 className="modal__heading">{props.header}</h2>
        <p className="modal__description">{props.text}</p>
        {props.closeButton ? (
          <div onClick={props.onCloseModal} className="modal-closeButton">
            <div className="closeButton-line"></div>
            <div className="closeButton-line"></div>
          </div>
        ) : null}

        <div className="modal__action-btn_container">
          {props.actions && props.actions}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  actions: PropTypes.element,
  onCloseModal: PropTypes.func,
};

export default Modal;
