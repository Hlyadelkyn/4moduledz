import React from "react";
import Item from "../item/Item";
import Button from "../button/Button";
import PropTypes from "prop-types";
import { toggleFavoriteItemId } from "../../state/actions/favoriteItemsIdList";
import { setCurrentItemId } from "../../state/actions/itemsList";
import { useStore, useDispatch } from "react-redux";

function ItemList({ itemButtonText, itemActionButtonType, list }) {
  let store = useStore();
  let dispatch = useDispatch();

  console.log(list);
  console.log(itemActionButtonType);

  return (
    <>
      <div className="items-container">
        {list.length > 0 &&
          list.map((item, id) => {
            let isInFavorites;

            if (
              store.getState().favoriteItemsIdList.indexOf(item.article) !== -1
            ) {
              isInFavorites = "item__favorite-button inFavorites";
            } else {
              isInFavorites = "item__favorite-button";
            }
            return (
              <Item
                key={id}
                name={item.name}
                price={item.price}
                imgSrc={item.img}
                article={item.article}
                color={item.color}
                favoriteBtn={
                  <Button
                    text={
                      <svg
                        className="favorite-button_svg "
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
                      </svg>
                    }
                    className={isInFavorites}
                    onClick={() => {
                      toggleFavoriteItemId(item);
                    }}
                  />
                }
                actionBtn={
                  <Button
                    text={itemButtonText}
                    className="item__buy-btn"
                    onClick={() => {
                      dispatch(itemActionButtonType(true));
                      setCurrentItemId(item.article);
                    }}
                  />
                }
              />
            );
          })}
      </div>
    </>
  );
}

ItemList.propTypes = {
  favoriteItems: PropTypes.array,
  itemsArray: PropTypes.array,
  onFavoriteButtonClick: PropTypes.func,
  onItemBuyButtonClick: PropTypes.func,
  itemButtonText: PropTypes.string,
};

export default ItemList;
