import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { useStore } from "react-redux";

function Navigation() {
  let store = useStore();
  return (
    <ul className="global-nav">
      <li className="global-nav__li">
        <NavLink to="/home">Home</NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/cart">
          cart{" "}
          <span className="itemsCount">
            {" "}
            {store.getState().cartItemsIdList.length}
          </span>
        </NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/favotite">
          favorite{" "}
          <span className="itemsCount">
            {" "}
            {store.getState().favoriteItemsIdList.length}
          </span>
        </NavLink>
      </li>
    </ul>
  );
}

Navigation.propTypes = {
  cartItemList: PropTypes.array,
  favoriteItemList: PropTypes.array,
};

export default Navigation;
