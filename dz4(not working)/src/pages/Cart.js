import React from "react";
import { useStore } from "react-redux";
import ItemList from "../components/itemList/ItemList";

function Cart({ favoriteItems, itemsArray }) {
  let store = useStore();
  return (
    <>
      <h1 className="page-title"> Cart</h1>

      <ItemList
        list={store.getState().itemsList.filter((item) => {
          if (store.getState().cartItemsIdList.indexOf(item.article) > -1) {
            return true;
          }
          return false;
        })}
        itemButtonText="remove from cart"
        itemActionButtonType="RemoveModalOpen/toggle"
      />
    </>
  );
}

export default Cart;
