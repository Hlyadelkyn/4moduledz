import React from "react";
import { useStore, useDispatch } from "react-redux";
import ItemList from "../components/itemList/ItemList";
import { buyModalSetState } from "../state/actions/modalsHandlers";

function Home() {
  let store = useStore();
  console.log(store.getState());
  return (
    <>
      <h1 className="page-title">Store</h1>
      <ItemList
        list={store.getState().itemsList}
        itemButtonText="add to cart"
        itemActionButtonType={buyModalSetState}
      />
    </>
  );
}

export default Home;
