import React from "react";
import { useStore } from "react-redux";
import ItemList from "../components/itemList/ItemList";
function Favorite({ itemsArray, favoriteItems }) {
  let store = useStore();
  return (
    <>
      <h1 className="page-title">Store</h1>
      <ItemList
        list={store.getState().itemsList.filter((item) => {
          if (store.getState().favoriteItemsIdList.indexOf(item.article) > -1) {
            return true;
          }
          return false;
        })}
        itemButtonText="add to cart"
        itemActionButtonType="BuyModal/toggle"
      />
    </>
  );
}

export default Favorite;
