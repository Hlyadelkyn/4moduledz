import "./App.css";
import React from "react";
import {
BrowserRouter as Router,
Switch,
Redirect,
Route,
} from "react-router-dom";
import Navigation from "./components/navigation/Navigation";
import { connect, useStore } from "react-redux";

import Home from "./pages/Home";
import Favorite from "./pages/Favorite";
import Cart from "./pages/Cart";

import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";

class App extends React.Component {
// const dispatch = useDispatch()
componentDidMount() {
fetch("./itemsArr.json")
.then((data) => data.json())
.then((arr) => {
this.setState({
itemList: arr,
});
store.dispatch({ type: "itemsList/itemsSet", payload: arr });
});
if (localStorage.getItem("cart") !== undefined) {
store.dispatch({
type: "cartItemList/itemsSet",
payload: JSON.parse(localStorage.getItem("cart")),
});
}
if (localStorage.getItem("favoriteItems")) {
store.dispatch({
type: "favoriteItems/itemsSet",
payload: JSON.parse(localStorage.getItem("favoriteItems")),
});
}
}

handleItemAdd = () => {
store.dispatch({
type: "BuyModal/toggle",
});
Promise.resolve(
(() => {
let tempCartItemsArticles = store
.getState()
.cartItemList.map((item) => item.article);
let currentItem = this.props.currentItem;
if (!(tempCartItemsArticles.indexOf(currentItem.article) > -1)) {
return store.dispatch({
type: "cartItemList/add",
payload: currentItem,
});
}
})()
).then(() => {
localStorage.setItem("cart", JSON.stringify(this.props.cartItemList));
store.dispatch({
type: "currentItem/del",
});
});
};
handleItemRemove = () => {
store.dispatch({
type: "cartItemList/remove",
payload: this.props.currentItem,
});
};

handleBuyModalButtonClick = () => {
store.dispatch({
type: "BuyModal/toggle",
});
};
handleRemoveModalButtonClick = () => {
store.dispatch({
type: "RemoveModalOpen/toggle",
});
};
handleItemBuyButtonClick = (item) => {
store.dispatch({
type: "BuyModal/toggle",
});
store.dispatch({
type: "currentItem/set",
payload: item,
});
};
handleItemRemoveButtonClick = (item) => {
store.dispatch({
type: "RemoveModal/toggle",
});
};

render() {
return (

<div className="App">
<Navigation />
{this.props.isRemoveModalOpen && (
<Modal
header="Do you want to remove this book from cart?"
closeButton={false}
actions={
<>
<Button
text="remove"
className="modal**action-button"
onClick={() => {
this.handleItemRemove();
}}
/>
<Button
text="cancel"
className="modal**action-button"
onClick={() => {
this.handleRemoveModalButtonClick();
}}
/>
</>
}
onCloseModal={this.handleRemoveModalButtonClick}
/>
)}
{this.props.isBuyModalOpen && (
<Modal
header="Do you want to add this book to cart?"
closeButton={true}
actions={
<>
<Button
text="add"
className="modal**action-button"
onClick={() => {
this.handleItemAdd();
}}
/>
<Button
text="cancel"
className="modal**action-button"
onClick={() => {
this.handleItemBuyButtonClick();
}}
/>
</>
}
onCloseModal={this.handleBuyModalButtonClick}
/>
)}
<Switch>
<Redirect exact from="/" to="/home" />

          <Route path="/home">
            <Home
              onHandleBuyModalButtonClick={this.handleBuyModalButtonClick}
              onHandleItemAdd={this.handleItemAdd}
            />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/favotite">
            <Favorite />
          </Route>
          <Route path="/*">
            <>
              <h1 className="not-found">oops, smth went wrong</h1>
            </>
          </Route>
        </Switch>
      </div>
    );

}
}
const mapStateToProps = (state) => {
return {
itemsArray: state.itemList,
favoriteItems: state.favoriteItems,
cartItemList: state.cartItemList,
isBuyModalOpen: state.isBuyModalOpen,
isRemoveModalOpen: state.isRemoveModalOpen,
currentItem: state.currentItem,
};
};

export default connect(mapStateToProps)(App);

case "itemsList/itemsSet":
return {
...state,

        itemList: action.payload,
      };
    case "BuyModal/toggle":
      let tempIsBuyModalState = !state.isBuyModalOpen;
      return {
        ...state,
        isBuyModalOpen: tempIsBuyModalState,
      };
    case "RemoveModalOpen/toggle":
      let tempIsRemoveModalStat = !state.isRemoveModalOpen;
      return {
        ...state,
        isRemoveModalOpen: tempIsRemoveModalStat,
      };
    case "currentItem/set":
      return {
        ...state,
        currentItem: action.payload,
      };
    case "currentItem/del":
      return {
        ...state,
        currentItem: "",
      };
    case "cartItemList/add":
      return {
        ...state,
        cartItemList: [...state.cartItemList, action.payload],
      };
    case "favoriteItems/toggleItem":
