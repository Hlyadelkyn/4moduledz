import "./App.css";
import React, { useEffect, useState } from "react";
import ItemList from "./components/itemList/ItemList";
import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";
import {
BrowserRouter as Router,
Switch,
Redirect,
Route,
} from "react-router-dom";
import Navigation from "./components/navigation/Navigation";

function App() {
let [itemList, setItemList] = useState([]);
let [cartItemList, setCartItemList] = useState([]);
let [isBuyModalOpen, setIsBuyModalOpen] = useState(false);
let [isRemoveModalOpen, setIsRemoveModalOpen] = useState(false);
let [favoriteItems, setFavoriteItems] = useState([]);
let [currentItem, setCurrentItem] = useState("");

useEffect(() => {
fetch("./itemsArr.json")
.then((data) => data.json())
.then((arr) => {
setItemList(arr);
});
if (localStorage.getItem("cart")) {
setCartItemList(JSON.parse(localStorage.getItem("cart")));
}
if (localStorage.getItem("favoriteItems")) {
setFavoriteItems(JSON.parse(localStorage.getItem("favoriteItems")));
}
});

let handleItemAdd = () => {
setIsBuyModalOpen((prevState) => {
return !prevState.isBuyModalOpen;
});
Promise.resolve(
setCartItemList((prevState) => {
if (!(prevState.indexOf(currentItem) > -1)) {
return [...prevState, currentItem];
}
})
).then(() => {
localStorage.setItem("cart", JSON.stringify(cartItemList));
setCurrentItem("");
});
};
let handleItemRemove = () => {
setIsRemoveModalOpen((prevState) => {
return !prevState.isRemoveModalOpen;
});

    Promise.resolve(
      setCartItemList((prevState) => {
        let cartItemNames = prevState.map((item) => item.name);
        if (cartItemNames.indexOf(currentItem.name) > -1) {
          prevState.splice(prevState.indexOf(currentItem), 1);

          return prevState;
        }
      })
    ).then(() => {
      localStorage.setItem("cart", JSON.stringify(cartItemList));
      setCurrentItem("");
    });

};

let handleFavoriteButtonClick = (name) => {
Promise.resolve(
setFavoriteItems((prevState) => {
if (!(prevState.indexOf(name) > -1)) {
return [...prevState, name];
} else {
prevState.splice(prevState.indexOf(name), 1);
return prevState.favoriteItems;
}
})
).then(() => {
localStorage.setItem("favoriteItems", JSON.stringify(favoriteItems));
});
};
let handleBuyModalButtonClick = () => {
setIsBuyModalOpen((prevState) => {
return !prevState;
});
};
let handleRemoveModalButtonClick = () => {
setIsRemoveModalOpen((prevState) => {
return !prevState;
});
};
let handleItemBuyButtonClick = (item) => {
setIsBuyModalOpen((prevState) => {
return !prevState;
});
setCurrentItem(item);
};
let handleItemRemoveButtonClick = (item) => {
setIsRemoveModalOpen((prevState) => {
return !prevState;
});
setCurrentItem(item);
};

return (
<Router>

<div className="App">
<Navigation />
<Switch>
<Redirect exact from="/" to="/home" />

          <Route path="/home">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemAdd();
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemBuyButtonClick();
                        }}
                      />
                    </>
                  }
                  onCloseModal={() => {
                    handleBuyModalButtonClick();
                  }}
                />
              )}
              <h1 className="page-title">Store</h1>
              <ItemList
                itemsArray={itemList}
                onItemButtonClick={() => {
                  handleItemBuyButtonClick();
                }}
                onFavoriteButtonClick={() => {
                  handleFavoriteButtonClick();
                }}
                favoriteItems={favoriteItems}
                itemButtonText="add to cart"
              />
            </>
          </Route>
          <Route path="/cart">
            {isRemoveModalOpen && (
              <Modal
                header="Do you want to remove this book from cart?"
                closeButton={false}
                actions={
                  <>
                    <Button
                      text="remove"
                      className="modal__action-button"
                      onClick={() => {
                        handleItemRemove();
                      }}
                    />
                    <Button
                      text="cancel"
                      className="modal__action-button"
                      onClick={() => {
                        handleRemoveModalButtonClick();
                      }}
                    />
                  </>
                }
                onCloseModal={handleRemoveModalButtonClick}
              />
            )}
            <h1 className="page-title">Cart</h1>

            <ItemList
              itemsArray={cartItemList}
              onItemButtonClick={() => {
                handleItemRemoveButtonClick();
              }}
              onFavoriteButtonClick={() => {
                handleFavoriteButtonClick();
              }}
              favoriteItems={favoriteItems}
              itemButtonText="remove from cart"
            />
          </Route>
          <Route path="/favotite">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemAdd();
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemBuyButtonClick();
                        }}
                      />
                    </>
                  }
                  onCloseModal={() => {
                    handleBuyModalButtonClick();
                  }}
                />
              )}
              <h1 className="page-title">Favorite</h1>
              <ItemList
                itemsArray={favoriteItems}
                onItemButtonClick={() => {
                  handleItemBuyButtonClick();
                }}
                onFavoriteButtonClick={() => {
                  handleFavoriteButtonClick();
                }}
                favoriteItems={favoriteItems}
                itemButtonText="add to cart"
              />
            </>
          </Route>
          <Route path="/*">
            <>
              <h1 className="not-found">oops, smth went wrong</h1>
            </>
          </Route>
        </Switch>
      </div>
    </Router>

);
}

export default App;

import "./App.css";
import React from "react";
import ItemList from "./components/itemList/ItemList";
import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";
import {
BrowserRouter as Router,
Switch,
Redirect,
Route,
} from "react-router-dom";
import Navigation from "./components/navigation/Navigation";

class App extends React.Component {
state = {
itemList: [],
cartItemList: [],
isBuyModalOpen: false,
isRemoveModalOpen: false,
favoriteItems: [],
currentItem: "",
};
componentDidMount() {
fetch("./itemsArr.json")
.then((data) => data.json())
.then((arr) => {
this.setState({
itemList: arr,
});
});
if (localStorage.getItem("cart")) {
this.setState({ cartItemList: JSON.parse(localStorage.getItem("cart")) });
}
if (localStorage.getItem("favoriteItems")) {
this.setState(() => {
return {
favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")),
};
});
}
}

handleItemAdd = () => {
this.setState((prevState) => {
return { isBuyModalOpen: !prevState.isBuyModalOpen };
});
Promise.resolve(
this.setState((prevState) => {
let tempCartItemsArticles = prevState.cartItemList.map(
(item) => item.article
);
if (
!(tempCartItemsArticles.indexOf(this.state.currentItem.article) > -1)
) {
return {
cartItemList: [...prevState.cartItemList, this.state.currentItem],
};
}
})
).then(() => {
localStorage.setItem("cart", JSON.stringify(this.state.cartItemList));
this.setState({ currentItem: "" });
});
};
handleItemRemove = () => {
this.setState((prevState) => {
return { isRemoveModalOpen: !prevState.isRemoveModalOpen };
});

    Promise.resolve(
      this.setState((prevState) => {
        let cartItemNames = prevState.cartItemList.map((item) => item.article);
        if (cartItemNames.indexOf(this.state.currentItem.article) > -1) {
          prevState.cartItemList.splice(
            prevState.cartItemList.indexOf(this.state.currentItem),
            1
          );

          return {
            cartItemList: prevState.cartItemList,
          };
        }
      })
    ).then(() => {
      localStorage.setItem("cart", JSON.stringify(this.state.cartItemList));
      this.setState({ currentItem: "" });
    });

};

handleFavoriteButtonClick = (item) => {
Promise.resolve(
this.setState((prevState) => {
let tempFavoriteItemsArticles = prevState.favoriteItems.map(
(item) => item.article
);
if (!(tempFavoriteItemsArticles.indexOf(item.article) > -1)) {
return { favoriteItems: [...prevState.favoriteItems, item] };
} else {
prevState.favoriteItems.splice(
tempFavoriteItemsArticles.indexOf(item.article),
1
);
return {
favoriteItems: prevState.favoriteItems,
};
}
})
).then(() => {
localStorage.setItem(
"favoriteItems",
JSON.stringify(this.state.favoriteItems)
);
});
};
handleBuyModalButtonClick = () => {
this.setState((prevState) => {
return { isBuyModalOpen: !prevState.isBuyModalOpen };
});
};
handleRemoveModalButtonClick = () => {
this.setState((prevState) => {
return { isRemoveModalOpen: !prevState.isRemoveModalOpen };
});
};
handleItemBuyButtonClick = (item) => {
this.setState((prevState) => {
return {
isBuyModalOpen: !prevState.isBuyModalOpen,
currentItem: item,
};
});
};
handleItemRemoveButtonClick = (item) => {
this.setState((prevState) => {
return {
isRemoveModalOpen: !prevState.isRemoveModalOpen,
currentItem: item,
};
});
};

render() {
return (
<Router>

<div className="App">
<Navigation
            cartItemList={this.state.cartItemList}
            favoriteItemList={this.state.favoriteItems}
          />
<Switch>
<Redirect exact from="/" to="/home" />

            <Route path="/home">
              <>
                {this.state.isBuyModalOpen && (
                  <Modal
                    header="Do you want to add this book to cart?"
                    closeButton={true}
                    actions={
                      <>
                        <Button
                          text="add"
                          className="modal__action-button"
                          onClick={() => {
                            this.handleItemAdd();
                          }}
                        />
                        <Button
                          text="cancel"
                          className="modal__action-button"
                          onClick={() => {
                            this.handleItemBuyButtonClick();
                          }}
                        />
                      </>
                    }
                    onCloseModal={this.handleBuyModalButtonClick}
                  />
                )}
                <h1 className="page-title">Store</h1>
                <ItemList
                  itemsArray={this.state.itemList}
                  onItemButtonClick={this.handleItemBuyButtonClick}
                  onFavoriteButtonClick={this.handleFavoriteButtonClick}
                  favoriteItems={this.state.favoriteItems}
                  itemButtonText="add to cart"
                />
              </>
            </Route>
            <Route path="/cart">
              {this.state.isRemoveModalOpen && (
                <Modal
                  header="Do you want to remove this book from cart?"
                  closeButton={false}
                  actions={
                    <>
                      <Button
                        text="remove"
                        className="modal__action-button"
                        onClick={() => {
                          this.handleItemRemove();
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          this.handleRemoveModalButtonClick();
                        }}
                      />
                    </>
                  }
                  onCloseModal={this.handleRemoveModalButtonClick}
                />
              )}
              <h1 className="page-title">Cart</h1>

              <ItemList
                itemsArray={this.state.cartItemList}
                onItemButtonClick={this.handleItemRemoveButtonClick}
                onFavoriteButtonClick={this.handleFavoriteButtonClick}
                favoriteItems={this.state.favoriteItems}
                itemButtonText="remove from cart"
              />
            </Route>
            <Route path="/favotite">
              <>
                {this.state.isBuyModalOpen && (
                  <Modal
                    header="Do you want to add this book to cart?"
                    closeButton={true}
                    actions={
                      <>
                        <Button
                          text="add"
                          className="modal__action-button"
                          onClick={() => {
                            this.handleItemAdd();
                          }}
                        />
                        <Button
                          text="cancel"
                          className="modal__action-button"
                          onClick={() => {
                            this.handleItemBuyButtonClick();
                          }}
                        />
                      </>
                    }
                    onCloseModal={this.handleBuyModalButtonClick}
                  />
                )}
                <h1 className="page-title">Favorite</h1>
                <ItemList
                  itemsArray={this.state.favoriteItems}
                  onItemButtonClick={this.handleItemBuyButtonClick}
                  onFavoriteButtonClick={this.handleFavoriteButtonClick}
                  favoriteItems={this.state.favoriteItems}
                  itemButtonText="add to cart"
                />
              </>
            </Route>
            <Route path="/*">
              <>
                <h1 className="not-found">oops, smth went wrong</h1>
              </>
            </Route>
          </Switch>
        </div>
      </Router>
    );

}
}

export default App;
