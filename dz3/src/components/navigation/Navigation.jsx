import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

function Navigation(props) {
  return (
    <ul className="global-nav">
      <li className="global-nav__li">
        <NavLink to="/home">Home</NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/cart">
          cart <span className="itemsCount"> {props.cartItemList.length}</span>
        </NavLink>
      </li>
      <li className="global-nav__li">
        <NavLink to="/favotite">
          favorite{" "}
          <span className="itemsCount"> {props.favoriteItemList.length}</span>
        </NavLink>
      </li>
    </ul>
  );
}

Navigation.propTypes = {
  cartItemList: PropTypes.array,
  favoriteItemList: PropTypes.array,
};

export default Navigation;
