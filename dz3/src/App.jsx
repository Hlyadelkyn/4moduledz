import "./App.css";
import React, { useEffect, useState } from "react";
import ItemList from "./components/itemList/ItemList";
import Modal from "./components/Modal/Modal";
import Button from "./components/button/Button";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route,
} from "react-router-dom";
import Navigation from "./components/navigation/Navigation";

const App = () => {
  const [itemList, setItemList] = useState([]);
  const [cartItemList, setCartItemList] = useState([]);
  const [isBuyModalOpen, setIsBuyModalOpen] = useState(false);
  const [isRemoveModalOpen, setIsRemoveModalOpen] = useState(false);
  const [favoriteItems, setFavoriteItems] = useState([]);
  const [currentItem, setCurrentItem] = useState("");
  useEffect(() => {
    fetch("./itemsArr.json")
      .then((data) => data.json())
      .then((arr) => {
        setItemList(arr);
      });
    if (
      localStorage.getItem("cart") &&
      localStorage.getItem("cart") !== undefined
    ) {
      setCartItemList(JSON.parse(localStorage.getItem("cart")));
    }
    if (localStorage.getItem("favoriteItems")) {
      setFavoriteItems(JSON.parse(localStorage.getItem("favoriteItems")));
    }
  }, []);

  let setLCJSON = (propName, value) => {
    localStorage.setItem(propName, JSON.stringify(value));
    // console.log(value);
  };

  let handleItemAdd = () => {
    setCartItemList((prevState) => {
      if (
        !(
          cartItemList
            .map((item) => item.article)
            .indexOf(currentItem.article) > -1
        )
      ) {
        return [...prevState, currentItem];
      } else {
        return prevState;
      }
    });

    handleBuyModalButtonClick();
  };

  useEffect(() => {
    console.log(cartItemList);
    if (cartItemList.length > 0) {
      setLCJSON("cart", cartItemList);
    }
  }, [cartItemList]);

  let handleItemRemove = () => {
    setCartItemList((prevState) => {
      if (prevState)
        if (
          prevState.map((item) => item.article).indexOf(currentItem.article) >
          -1
        ) {
          return prevState.filter((el) => el.article !== currentItem.article);
        } else {
          return prevState;
        }
    });
    setIsRemoveModalOpen(false);
  };

  let handleItemBuyButtonClick = (item) => {
    setIsBuyModalOpen(true);
    setCurrentItem(item);
  };
  let handleItemRemoveButtonClick = (item) => {
    setIsRemoveModalOpen(true);
    setCurrentItem(item);
  };

  let handleBuyModalButtonClick = () => {
    setIsBuyModalOpen(false);
  };
  let handleRemoveModalButtonClick = () => {
    setIsRemoveModalOpen(false);
  };
  let handleFavoriteButtonClick = (item) => {
    setFavoriteItems((prevState) => {
      if (prevState.map((el) => el.article).indexOf(item.article) < 0) {
        return [...prevState, item];
      } else {
        return prevState.filter((el) => el.article !== item.article);
      }
    });
  };

  useEffect(() => {
    if (favoriteItems.length > 0) {
      setLCJSON("favoriteItems", favoriteItems);
    }
  }, [favoriteItems]);

  return (
    <Router>
      <div className="App">
        <Navigation
          cartItemList={cartItemList}
          favoriteItemList={favoriteItems}
        />
        <Switch>
          <Redirect exact from="/" to="/home" />

          <Route path="/home">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemAdd();
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          handleBuyModalButtonClick();
                        }}
                      />
                    </>
                  }
                  onCloseModal={handleBuyModalButtonClick}
                />
              )}
              <h1 className="page-title">Store</h1>
              <ItemList
                itemsArray={itemList}
                onItemButtonClick={handleItemBuyButtonClick}
                onFavoriteButtonClick={handleFavoriteButtonClick}
                favoriteItems={favoriteItems}
                itemButtonText="add to cart"
              />
            </>
          </Route>
          <Route path="/cart">
            {isRemoveModalOpen && (
              <Modal
                header="Do you want to remove this book from cart?"
                closeButton={false}
                actions={
                  <>
                    <Button
                      text="remove"
                      className="modal__action-button"
                      onClick={() => {
                        handleItemRemove();
                      }}
                    />
                    <Button
                      text="cancel"
                      className="modal__action-button"
                      onClick={() => {
                        handleRemoveModalButtonClick();
                      }}
                    />
                  </>
                }
                onCloseModal={handleRemoveModalButtonClick}
              />
            )}
            <h1 className="page-title">Cart</h1>

            <ItemList
              itemsArray={cartItemList}
              onItemButtonClick={handleItemRemoveButtonClick}
              onFavoriteButtonClick={handleFavoriteButtonClick}
              favoriteItems={favoriteItems}
              itemButtonText="remove from cart"
            />
          </Route>
          <Route path="/favotite">
            <>
              {isBuyModalOpen && (
                <Modal
                  header="Do you want to add this book to cart?"
                  closeButton={true}
                  actions={
                    <>
                      <Button
                        text="add"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemAdd();
                        }}
                      />
                      <Button
                        text="cancel"
                        className="modal__action-button"
                        onClick={() => {
                          handleItemBuyButtonClick();
                        }}
                      />
                    </>
                  }
                  onCloseModal={handleBuyModalButtonClick}
                />
              )}
              <h1 className="page-title">Favorite</h1>
              <ItemList
                itemsArray={favoriteItems}
                onItemButtonClick={handleItemBuyButtonClick}
                onFavoriteButtonClick={handleFavoriteButtonClick}
                favoriteItems={favoriteItems}
                itemButtonText="add to cart"
              />
            </>
          </Route>
          <Route path="/*">
            <>
              <h1 className="not-found">oops, smth went wrong</h1>
            </>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};
export default App;
